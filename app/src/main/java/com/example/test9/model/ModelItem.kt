package com.example.test9.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ModelItem(
    @Json(name = "cover")
    val cover: String,
    @Json(name = "liked")
    val liked: Boolean,
    @Json(name = "price")
    val price: String,
    @Json(name = "title")
    val title: String
)