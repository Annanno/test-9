package com.example.test9.fragments




import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.GridLayoutManager
import com.example.test9.BaseFragment
import com.example.test9.ItemsViewModel
import com.example.test9.adapters.ItemsAdapter
import com.example.test9.databinding.FragmentGalleryBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


class GalleryFragment : BaseFragment<FragmentGalleryBinding>(FragmentGalleryBinding::inflate) {

    private lateinit var itemsAdapter: ItemsAdapter
    private val viewModel: ItemsViewModel by viewModels()
    @RequiresApi(Build.VERSION_CODES.M)
    override fun init() {
        viewModel.getData()
        initRecycler()
        checkConnection()



    }

    private fun initRecycler(){
        itemsAdapter = ItemsAdapter()
        binding.recycler.apply {
            adapter = itemsAdapter
            layoutManager = GridLayoutManager(requireContext(), 2)
        }
    }

    private fun observeApi(){
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED){
                viewModel.itemsFlow.collect {
                    itemsAdapter.setData(it.toMutableList())
                }
            }
        }

    }
    private fun observeDataBase(){
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED){
                viewModel.items.collect {
                    itemsAdapter.setData(it.toMutableList())
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun checkConnection(){
        if (isNetworkAvailable()){
            observeApi()
        }else
            observeDataBase()


    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun isNetworkAvailable(): Boolean {
        val cm = requireContext().getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)

        return (capabilities != null && capabilities.hasCapability(NET_CAPABILITY_INTERNET))

    }





}