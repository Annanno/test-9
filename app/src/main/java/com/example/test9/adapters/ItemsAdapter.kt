package com.example.test9.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.test9.databinding.ItemLayoutBinding
import com.example.test9.extensions.setImage
import com.example.test9.model.ModelItem
import com.example.test9.room.entity.Item

class ItemsAdapter: RecyclerView.Adapter<ItemsAdapter.ViewHolder>() {

    val items = mutableListOf<Item>()


    inner class ViewHolder(private val binding: ItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var model: Item
        fun onBind() {
            model = items[adapterPosition]
            binding.title.text = model.title
            binding.price.text = model.price
            binding.itemImage.setImage(model.cover)


        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        ItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()

    }

    override fun getItemCount() = items.size

    fun setData(newList: MutableList<Item>) {
        items.clear()
        items.addAll(newList)
        notifyDataSetChanged()

    }
}