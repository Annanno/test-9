package com.example.test9.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.test9.App
import com.example.test9.dao.ItemDao
import com.example.test9.room.entity.Item

@Database(entities = [Item::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun itemDao(): ItemDao

    companion object{
        val db by lazy {
            Room.databaseBuilder(
                App.appContext!!,
                AppDatabase::class.java, "users_database"
            ).build()
        }
    }
}