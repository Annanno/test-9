package com.example.test9.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.test9.room.entity.Item
import kotlinx.coroutines.flow.Flow

@Dao
interface ItemDao {
    @Query("SELECT * FROM items_table")
    fun getAll(): Flow<List<Item>>


    @Insert
    suspend fun insertAll(items: List<Item>)

}