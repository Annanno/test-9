package com.example.test9.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "items_table")
data class Item(
    @PrimaryKey(autoGenerate = true) val uid: Int = 0,
    @ColumnInfo(name = "cover") val cover: String?,
    @ColumnInfo(name = "liked") val liked: Boolean?,
    @ColumnInfo(name = "price") val price: String?,
    @ColumnInfo(name = "title") val title: String?,
)
