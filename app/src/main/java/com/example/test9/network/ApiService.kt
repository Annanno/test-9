package com.example.test9.network

import com.example.test9.model.ModelItem
import com.example.test9.room.entity.Item
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("v3/05d71804-4628-4269-ac03-f86e9960a0bb")
    suspend fun getItems(): List<Item>
}