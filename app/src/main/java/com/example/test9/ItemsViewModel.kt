package com.example.test9

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.test9.database.AppDatabase
import com.example.test9.network.RetrofitInstance
import com.example.test9.room.entity.Item
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class ItemsViewModel: ViewModel() {

   var itemsFlow: MutableStateFlow<List<Item>> = MutableStateFlow(listOf())
    val items: Flow<List<Item>> = AppDatabase.db.itemDao().getAll()

    fun getData(){
        viewModelScope.launch {

            val result = RetrofitInstance.retrofit.getItems()

            itemsFlow.value = result
            AppDatabase.db.itemDao().insertAll(result)





        }
    }
}